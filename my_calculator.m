%MATLAB 2020b
%name: my_calculator
%author: kbklosok
%date: 13.11.2020
%version: v1.1

%Part 1
%Defining constant
PLANCK_CONST = 6.62607015*10^-34;%in J*s
ans_1 = PLANCK_CONST/(2*pi);
res_1 = sprintf('\n\n Part 1\n PLANCK_CONST = 6.62607015*10^-34; ans_1 = PLANCK_CONST/(2*pi);\n h/2pi = %d J*s', ans_1);
disp(res_1)

%Part 2
%45 degrees = pi/4
ans_2 = sin((pi/4)/exp(1));
res_2 = sprintf('\n\n Part 2\n ans_2 = sin((pi/4)/exp(1));\n sin((pi/4)/e) = %d', ans_2);
disp(res_2)

%Part 3
ans_3 = hex2dec('0x0098d6')/(1.445*10^23);
res_3 = sprintf("\n\n Part 3\n ans_3 = hex2dec('0x0098d6')/(1.445*10^23);\n 0x0098d6/(1.445*10^23) = %d", ans_3);
disp(res_3)

%Part 4
ans_4 = sqrt(exp(1)-pi);
res_4 = sprintf('\n\n Part 4\n ans_4 = sqrt(exp(1)-pi);\n sqrt(e-pi) = %d + %fi', real(ans_4),imag(ans_4));
disp(res_4)

%Part 5
%converting pi to string with 12 digits to the right of the decimal point
%and displaying last character
text = sprintf('%.12f',pi);
ans_5 = text(end);
res_5 = sprintf("\n\n Part 5\n text = sprintf('.12f',pi);ans_5 = text(end);\n Pi 12th digit: %s", ans_5);
disp(res_5)

%Part 6
%defining date of birth and calculating elapsed time
date_of_birth = datetime(2000,04,13,9,00,00);% RRRR,MM,DD,hh,mm,ss;
elapsed_time = datetime('now')-date_of_birth;% in hours:minutes:seconds
ans_6 = sprintf("Hours since my birth: %.0f", hours(elapsed_time)); % displaying hours
res_6 = sprintf("\n\n Part 6\n date_of_birth = datetime(2000,04,13,9,00,00);elapsed_time = datetime('now')-date_of_birth;ans_6 = sprintf('Hours since my birth: .0f', hours(elapsed_time));\n %s", ans_6);
disp(res_6)

%Part 7
%Defining constant
Rz =   6.371*10^6; %Earth radius; in m
ans_7 = atan((exp(sqrt(7)/2 * log(Rz/10^8)))/hex2dec('0xaaff'));
res_7 = sprintf("\n\n Part 7\n Rz = 6.371*10^6;ans_7 = atan((exp(sqrt(7)/2 * log(Rz/10^8)))/hex2dec('0xaaff'));;\n atan((e^(sqrt(7)/2 * log(Rz/10^8)))/0xaaff) = %d", ans_7);
disp(res_7)

%Part 8
%Defining Avogardo constant
AVOGARDO_CONST = 6.02214076  * 10^23; % in mole^-1
N = 1/4 * 10^-6 * AVOGARDO_CONST; % amount of ethanol molecules
n = 9*N; %amount of atoms
res_8 = sprintf("\n\n Part 8\n AVOGARDO_CONST = 6.02214076  * 10^23;N = 1/4 * 10^-6 * AVOGARDO_CONST;n = 9*N \n Amount of atoms in 1/4*10^-6 mole of ethanol : %e", n);
disp(res_8)

%Part 9
C = 2*n; %amount of carbon atoms
C_13 = 1/101 * C; %amount of C13 atoms
ans_9 = (C13/n) * 1000; % promiles
res_9 = sprintf("\n\n Part 9\n C = 2*n;C_13 = 1/101 * C;ans_9 = (C13/n) * 1000; \n C13 atoms in 1/4*10^-6 mole of ethanol : %f promiles", ans_9);
disp(res_9)

